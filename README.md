# Uberspace Backup Script to Dropbox

## Connect via ssh to your uberspace
``` shell
cd /bin
curl "https://gitlab.com/retober/uberbackup/raw/master/backup.sh" -o backup.sh # download the backupscript
curl "https://raw.githubusercontent.com/andreafabrizi/Dropbox-Uploader/master/dropbox_uploader.sh" -o dropbox_uploader.sh # download the dropbox uploader
chmod +x backup.sh && chmod +x dropbox_uploader.sh # make the scripts executable
./dropbox_uploader.sh # To setup Dropbox API-Key
nano backup.sh # to set your username & projectname
crontab -e # to setup a cronjob for each backup task
```

## The Cronjob

``` bash
# BACKUP DB
@daily sh /home/USERNAME/bin/backup.sh db > /dev/null
# BACKUP HTML
0 3 * * MON sh /home/USERNAME/bin/backup.sh html > /dev/null
```