#!/bin/bash

##Variables
USER='USERNAME' #Uberspacename
DATE=`date +%d-%m-%Y` #Current time
PROJECT_NAME="PROJECTNAME"
## END Variables

##Check if backup folder exists and create it
if [ ! -d /home/$USER/backups ]
    then
    mkdir /home/$USER/backups
    echo "Backup-folder created"
fi
##END Check

##CHECK IF PARAMETER IS SET ELSE EXIT
if [ "$1" = "" ]
then
    echo "Please set html or db as parameter."
    exit 1
fi
               
##IF HTML PARAMETER IS SET BACKUP HTML
if [ "$1" = "html" ]
    then
    tar -zcvPf /home/$USER/backups/HTML-$PROJECT_NAME-$DATE.tar.gz /var/www/virtual/$USER/html/

    sh /home/$USER/bin/dropbox_uploader.sh upload /home/$USER/backups/HTML-$PROJECT_NAME-$DATE.tar.gz Backup/$PROJECT_NAME/

     ##Check if old backup exists
    WEEKSAGO=$(date +%d-%m-%Y -d"2 week ago")
    if [ -f /home/$USER/backups/HTML-$PROJECT_NAME-$WEEKSAGO.tar.gz ]
    then
        rm  /home/$USER/backups/HTML-$PROJECT_NAME-$WEEKSAGO.tar.gz
        sh /home/$USER/bin/dropbox_uploader.sh delete /Backup/$PROJECT_NAME/HTML-$PROJECT_NAME-$WEEKSAGO.tar.gz
    fi
    ##END delete
fi

##IF DB PARAMETER IS SET BACKUP DB
if [ "$1" = "db" ]
    then
    ##BUILD MYSQL BACKUP
    mysqldump --defaults-file=$HOME/.my.cnf --user=$USER --compact --comments --dump-date --quick --all-databases | gzip > "/home/$USER/backups/DB-$PROJECT_NAME-$DATE.sql.gz"
    ##END Backup

    ## UPLOAD TO DROPBOX
    sh /home/$USER/bin/dropbox_uploader.sh upload /home/$USER/backups/DB-$PROJECT_NAME-$DATE.sql.gz Backup/$PROJECT_NAME/
    ## END UPLOAD

    ##Check if old backup exists
    WEEKSAGO=$(date +%d-%m-%Y -d"2 week ago")
    if [ -f /home/$USER/backups/DB-$PROJECT_NAME-$WEEKSAGO.sql.gz ]
    then
        rm /home/$USER/backups/DB-$PROJECT_NAME-$WEEKSAGO.sql.gz
        sh /home/$USER/bin/dropbox_uploader.sh delete /Backup/$PROJECT_NAME/DB-$PROJECT_NAME-$WEEKSAGO.sql.gz
    fi
    ##END delete
fi